-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jun 13, 2015 at 11:41 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `loan-calculator`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `types_of_loan`
-- 

CREATE TABLE `types_of_loan` (
  `id` int(11) NOT NULL auto_increment,
  `type_of_loan` varchar(255) NOT NULL,
  `interest` float NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `types_of_loan`
-- 

INSERT INTO `types_of_loan` VALUES (1, 'Housing laon', 3.5);
INSERT INTO `types_of_loan` VALUES (2, 'Car loan', 4);
INSERT INTO `types_of_loan` VALUES (3, 'Spending loan', 7.5);

-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `user`
-- 

INSERT INTO `user` VALUES (1, 'admin', 'admin');

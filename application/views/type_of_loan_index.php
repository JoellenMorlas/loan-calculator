<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8">
<title>Loan Calculator</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- css -->

<link
	href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/step4.css"
	rel="stylesheet">
<link
	href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.css"
	rel="stylesheet">
<link
	href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap-theme.css"
	rel="stylesheet">

<link
	href="<?php echo base_url(); ?>bower_components/fontawesome/css/font-awesome.min.css"
	rel="stylesheet">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<script src="bower_components/bootstrap/js/modernizr.js"></script>


<script language="JavaScript">
	function validate_interest(interest)
	{
		var positive_numner = Math.abs(interest);

		if (interest.match(/^-\d+$/)) {
			frmaddtype.interest.value = positive_numner;
        } else {
        	alert("The interest must be number !");
        	frmaddtype.interest.value = positive_numner;
        }
		
	}
</script>
</head>

<body>
	<div id="main" role="main">
		<article class="block prose container">

			<div class="row">
				<div class="col-md-3 col-sm-3">	
				<?php include 'config/menu-sidebar.php';?>
				</div>

				<div class=" col-md-9 col-sm-9">
					<div class="panel panel-default">
						<div class="panel-body">

							<h2 class="text-center">
								<span class=""></span><strong> Manage
									types of loan </strong>
							</h2>

							<div class="row">
								<div class="col-md-5"
									style="padding-top: 10px; padding-bottom: 10px; cursor: pointer;">

									<a style="text-decoration: none; font-size: 18px;" data-toggle="modal"
										data-target="#myModal"><span class="fa fa-plus-circle"></span>
										Add</a>

									<div class="modal fade" id="myModal" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel"
										aria-hidden="true">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>

													<h4 class="text-left" id="myModalLabel">
														<span class="fa fa-plus-circle"></span> Add type of loan
													</h4>
												</div>

												<div class="modal-body">

													<form class="omb_loginForm"
														action="<?php echo base_url(); ?>index.php/Type_of_loan/add"
														autocomplete="off" method="post" name="frmaddtype">
														<div class="row omb_row-sm-offset-3">
															<div class="col-xs-12 col-sm-12">


																<div class="input-group">
																	<span class="input-group-addon">Type of loan</span> <input
																		type="text" class="form-control" name="type of loan"
																		required="required">
																</div>
																<span class="help-block"></span>

																<div class="input-group">
																	<span class="input-group-addon">Interest (%)&nbsp;</span>
																	<input type="text" class="form-control" name="interest"
																		required="required"
																		value="<?php echo set_value('interest');?>"
																		OnChange="validate_interest(this.value);">
																</div>
																<br>

																<button class="btn btn-sm btn-primary btn-block"
																	type="submit">Add</button>
															</div>
														</div>
													</form>
												</div>
											</div>
											</form>
										</div>
									</div>
								</div>
							</div>

							<div class="table-responsive">
								<table class="table table-hover table-striped table-bordered">
									<thead>
										<tr class="active">

											<th>#</th>
											<th class="center">Types of loan</th>
											<th class="center">Interest</th>
											<th class="center">Action</th>
										</tr>
									</thead>
									<tbody>
		<?php
		if (count ( $rs ) == 0) {
			echo "<tr><td colspan='4' align='center'>-- no data --</td></tr>";
		} else {
			foreach ( $rs as $r ) {
				
				?>
				<tr>
											<td><?php echo $r['id'];?></td>
											<td><?php echo $r['type_of_loan'];?></td>
											<td><?php echo $r['interest'];?></td>

											<td class="col-md-1 center">
				<?php
				echo anchor ( base_url () . 'index.php/type_of_loan/edit/' . $r ['id'], '<div class="fa fa-pencil-square-o fa-lg"></div>' );
				echo "&nbsp;";
				echo anchor ( base_url () . 'index.php/type_of_loan/delete/' . $r ['id'], '<div class="fa fa-times fa-lg"></div>', array (
						"onclick" => "javascript:return confirm('Are you sure to delete this item ?');" 
				) );
				?>
				</td>
				</tr>
			<?php
			}
		}
		
		?>
		</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
	<div style="padding-top: 50px;"></div>

</body>
</html>
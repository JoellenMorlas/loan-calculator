<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8">
<title>Housing Loan</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
			
	<!-- css -->
	 <link href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/step4.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">

	 <link href="<?php echo base_url(); ?>bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
	 
	 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
     <!-- Include all compiled plugins (below), or include individual files as needed -->
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
 
 	 <script src="bower_components/bootstrap/js/modernizr.js"></script>

</head>

<body>
    <div id="main" role="main" style="padding-bottom: 20px;"> 
		<article class="block prose container">
				<div class="row-fluid">
					<div class="col-md-8">

						<iframe width="100%" height="350" frameborder="0" scrolling="no"
							marginheight="0" marginwidth="0"
							src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3875.7196285280393!2d100.56165800000001!3d13.735417000000007!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e29efd48233603%3A0xc8a193ead14bdc46!2sWebOn+Thailand!5e0!3m2!1sth!2sth!4v1434010183170" width="600" height="450" frameborder="0" style="border:0"></iframe>
					</div><div class="visible-xs visible-sm"><br></div>

					<div class="col-md-4">

						<div class="panel panel-default">
							<div class="panel-heading">Location</div>
							<div class="panel-body">
														
								<small><strong>Address:</strong><br>
								Unit 1901, Level 19, Exchange Tower
								388 Sukhumvit Rd, Klongtoey, Khet Klongtoey
								Bangkok 10110, Thailand<br><br>
								<strong>Office:</strong> +66 (0) 2260 4120</small>

							</div>
						</div>
						
						
						<div class="panel panel-default">
							<div class="panel-heading">Facebook</div>
							<div class="panel-body">
							
							<div class="fb-page" data-href="https://www.facebook.com/WebOn.Thai" data-hide-cover="false" data-show-facepile="false" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/WebOn.Thai"><a href="https://www.facebook.com/WebOn.Thai">WebOn Thailand</a></blockquote></div></div>
							
							</div>
						</div>
					</div>
				</div>
			
		</article>
	</div>
	
	
	
<div id="fb-root"></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	
</body>
</html>
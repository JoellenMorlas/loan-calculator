<!DOCTYPE html>
<html class="no-js">
<head>
<meta charset="utf-8">
<title>Loan Calculator</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	
	<!-- css -->
	 <link href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/step4.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
	 <link href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet">

	 <link href="<?php echo base_url(); ?>bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet">
	 
	 <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
     <!-- Include all compiled plugins (below), or include individual files as needed -->
	 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
 
 	 <script src="bower_components/bootstrap/js/modernizr.js"></script>
		
	<script language="JavaScript">
	function resutName(type_of_loan)
	{
		var x = type_of_loan;
		switch (x){
		<?php
		$sql = "select * from types_of_loan";
		$results = mysql_query ( $sql );
		while ( $result = mysql_fetch_array ( $results ) ) {
			?>
				case '<?=$result['id']?>' :
					frmCalculator.interest.value = '<?=$result['interest']?>';
					break;	
				<?php }?>

				default:
					frmCalculator.interest.value = ''
					break;
		}
	}
</script>
</head>

<body>
	<div id="main" role="main">
		<article class="block prose container">

			<div class="row">
				<div class="col-md-3 col-sm-3">	
					<?php include 'config/menu-sidebar.php';?>
				</div>

				<div class=" col-md-9 col-sm-9">
					<div class="panel panel-default">
						<div class="panel-body">

							<h2 class="text-center">
								<span class="fa fa-calculator fa-lg"></span><strong> Loan
									Calculation</strong>
							</h2>
							<br>

							<form class="form-horizontal col-md-7"
								action="<?php echo base_url(); ?>index.php/home/calculate"
								method="post" name="frmCalculator">

								<div class="form-group">
									<span class="col-md-4 control-label">Loan type </span>
									<div class="col-md-7">
										<div class="dropdown">

											<select class="form-control input-sm" name="type_of_loan"
												OnChange="resutName(this.value);">

												<option>-- Select type --</option>
											<?php foreach ($rs as $r) { ?>
											<option value="<?=$r['id'] ?>">
											
											<?php echo $r['type_of_loan']?></option> 
											<?php } ?>
											</select><?php echo form_error('type_of_loan',"<font color='error'>","</font>"); ?>
										</div>
									</div>
								</div>

								<div class="form-group">
									<span class="col-md-4 control-label">Interest (%)</span>
									<div class="col-md-7">
										<input type="text" class="form-control input-sm"
											name="interest" readonly="readonly"
											value='<?php echo set_value('interest');?>'>
									</div>
								</div>



								<div class="form-group">
									<span class="col-md-4 control-label">Amount </span>
									<div class="col-md-7">
										<input type="number" class="form-control input-sm"
											name="amount" value='<?php echo set_value('amount');?>'>
										<?php echo form_error('amount',"<font color='error'>","</font>"); ?>
									</div>
								</div>


								<div class="form-group">
									<span class="col-md-4 control-label">Payback time (year)</span>
									<div class="col-md-7">
										<input type="number" class="form-control input-sm"
											name="paybacktime"
											value='<?php echo set_value('paybacktime');?>'>
										<?php echo form_error('paybacktime',"<font color='error'>","</font>"); ?> 
									</div>
								</div>


								<div class="form-group">
									<div class="col-md-offset-4 col-md-8">
										<button type="submit" class="btn btn-default">Calculate</button>
										<button type="reset" class="btn btn-default">Reset</button>
									</div>
								</div>
							</form>


							<div class="col-md-5">
								<div class="panel panel-default">
									<div class="panel-body">
										<span class="control-label col-md-12 col-xs-12"
											style="border-bottom: 1px solid gray;">Result :</span><br>

										<div class="form-group">
											<br> <span class="col-md-7 col-xs-12 control-label">Monthly
												payback :</span>
											<div class="col-md-5 col-xs-12">
										<?php
										if (isset ( $monthly_payback )) {
											echo number_format ( $monthly_payback, 2 ) . " $";
										}
										?>
									</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
</body>
</html>
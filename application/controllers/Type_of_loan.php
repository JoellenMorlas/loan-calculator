<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type_of_loan extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model ( 'types_of_loan_model' );
	}
	
	public function index(){	
		
		// get all data type of loan
		$get_data = new types_of_loan_model();
		$data['rs'] = $get_data->get_data();
		
		// load view
		$this->load->view('config/header');
		$this->load->view('type_of_loan_index' , $data);
		$this->load->view('config/footer');
	}	
	
	public function add(){
		
			// Setting values
			$data = array (
					'type_of_loan' => $this->input->post ( "type_of_loan" ),
					'interest' => $this->input->post ( "interest" ),
			);
			
			// Transfering Data To Model
			$this->db->insert ( 'types_of_loan', $data );
			// Loading view
			 redirect('Type_of_loan', 'refresh');
				
		}
	
		
	public function edit($id){

		if ($this->input->post ( "btsave" ) != null) {
	
			$ar = array (
					'type_of_loan' => $this->input->post ( "type_of_loan" ),
					'interest'     => $this->input->post ( "interest" )
			);
				
			$this->db->where ( "id", $id );
			$this->db->update ( 'types_of_loan', $ar );
			redirect('Type_of_loan', 'refresh');
			exit ();
		}
		
		$sql = "Select * from types_of_loan where id='$id'";
		$rs = $this->db->query ( $sql );
		
		if ($rs->num_rows () == 0) {
			$data ['rs'] = array ();
		} else {
			$data ['rs'] = $rs->row_array ();
		}
		
		$this->load->view('config/header');
		$this->load->view('type_of_loan_update',$data);
		$this->load->view('config/footer');
		
		
	}
	
	public function delete($id) {

		$this->db->delete ( 'types_of_loan', array (
				'id' => $id
		));
		
		 redirect('Type_of_loan', 'refresh');
	}
	
}

<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Home extends CI_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->helper ( 'url' );
		$this->load->model ( 'types_of_loan_model' );
	}
	public function calculate() {
		
		// load library
		$this->load->library ( 'form_validation' );
		$this->form_validation->set_error_delimiters ( '<div class="error">', '</div>' );
		
		// Validating Name Field
		$this->form_validation->set_rules('type_of_loan','type of loan','required|greater_than[0]');
		$this->form_validation->set_rules ( 'amount', 'amount', 'required|is_natural_no_zero' );
		$this->form_validation->set_rules ( 'paybacktime', 'payback time in year', 'required|is_natural_no_zero' );
		
		// set message
		$this->form_validation->set_message('greater_than', 'The type of loan field must selected');
		
		// get data for dropdown
		$dropdown = new types_of_loan_model ();
		$data ['rs'] = $dropdown->get_data ();
		
		// if false
		if ($this->form_validation->run () == FALSE) {
			
			$this->load->view ( 'config/header' );
			$this->load->view ( 'home', $data );
			$this->load->view ( 'config/footer' );
		} 		

		// if Pass
		else {
			// get value form view
			$amount = $_REQUEST ['amount'];
			$payback = $_REQUEST ['paybacktime'];
			$interest = $_REQUEST ['interest'];
			$type_of_loan = $_REQUEST ['type_of_loan'];
			
			switch ($type_of_loan) {
				case 1 :
					$monthly_payback = $this->house_calculator ( $amount, $payback, $interest );
					$data ['monthly_payback'] = $monthly_payback;
					
					$this->load->view ( 'config/header' );
					$this->load->view ( 'home', $data );
					$this->load->view ( 'config/footer' );
					break;
				
				case 2 :
					
					$data ['monthly_payback'] = null;
					
					$this->load->view ( 'config/header' );
					$this->load->view ( 'home', $data );
					$this->load->view ( 'config/footer' );
					break;
				
				case 3 :
					
					$data ['monthly_payback'] = null;
					
					$this->load->view ( 'config/header' );
					$this->load->view ( 'home', $data );
					$this->load->view ( 'config/footer' );
					break;
					
				default:
					$this->load->view ( 'config/header' );
					$this->load->view ( 'home', $data );
					$this->load->view ( 'config/footer' );
					break;
			}
		}
	}
	public function house_calculator($amount, $payback, $interest) {
		
		// interest per year
		$i = $interest / 1200;
		// payback time (year) / 1 year = 12 months
		$t = $payback * 12;
		
		$v = pow ( (1 / (1 + $i)), $t );
		$r = ($i) / (1 - $v) * $amount;
		
		return $monthly_payback = $r;
	}
	
	public function car_calculator() {
		
	}
	
	public function spenging_calculator() {
		
	}
	
}
